const fs = require('fs')

fs.readFile('./itemInfo.lua', 'latin1',(err, data) => {
    if(err) return console.log(err)
    scrapArray(data);
})

const scrapArray = (data) => {
    let arr = data;

    textForm(arr.split('tbl = {')[1].split('} \n function main()')[0])
}

const textForm = (data) => {
    let items = data;
    let fitem = [];
    let ids = [];
    // create a array of items
    items = items.replaceAll('},\n	[', '},\n	#[').split('#').filter(Boolean)
    // get the ids
    items.forEach(e => {
        ids.push(e.split(' = ')[0])
    })
    // optional: format the id
    ids[0] = ids[0].split('\n')[1].trim();
    // console.log()
    ids.forEach((e,i) => {
        // ids[i] = `ItemID: ${e.split('[').join('').split(']').join('')}`;
        ids[i] = `Item ID: ^0000FF ${e.split('[').join('').split(']').join('')}^000000`;
    })
    // put the id on the string
    items.forEach((e, i) => {
        fitem.push(e.split('identifiedDescriptionName = {\n').join(`identifiedDescriptionName = {\n        	"${ids[i]}",\n`))
    })
    // write file
    fs.writeFile('./_itemInfo.lua', `tbl = {${fitem.join("")}`, 'latin1' ,(err) => {
        if(err) return console.log(err)
        console.log('done');
    })
}